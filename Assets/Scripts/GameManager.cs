﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public Text clock;
	int time = 3;
	public static bool run;
	// Use this for initialization
	void Start () {
		run = false;
		StartCoroutine (StarGame ());
	}

	IEnumerator StarGame()
	{
		while (true) {
			if (time > 0) {
				clock.text = time.ToString ();
			} else {
				run = true;
				Destroy (clock);
			}
			time--;
		}
		//yield return new WaitForSeconds ();

	}
}
