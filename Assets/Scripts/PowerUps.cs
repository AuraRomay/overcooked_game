﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUps : MonoBehaviour
{

	public int m_PlayerNumber;
	private string m_Y;
	public float m_YV;
    public float boosPower;
    public float coolDown;
    public bool isPlaying = true;

    // Use this for initialization
    void Start()
    {
		m_Y ="Y"+ m_PlayerNumber;
    }

    // Update is called once per frame
    void Update()
    {
        Boost();

    }

    void Boost()
    {
		if (Input.GetButtonDown(m_Y) && isPlaying)
        {
            Debug.Log("Laura");
			GetComponent<PlayerMovement>().dash = boosPower;
            StartCoroutine("PlayCool");
        }
        else
        {
            GetComponent<PlayerMovement>().dash = 1;
        }
    }

    IEnumerator PlayCool()
    {
        isPlaying = false;
        yield return new WaitForSeconds(coolDown);
        isPlaying = true;
    }
}
