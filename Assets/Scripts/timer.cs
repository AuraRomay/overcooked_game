﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class timer : MonoBehaviour
{
    public Text timerText;
    [Range(3.0f, 600.0f)]
    private bool onlyShowingSeconds = false;
    private bool startTimer = true;
    public float t = 600f;

    //public float n_time = 0f;

    void Start()
    {
        //startTime = Time.time; 
    }

    void Update()
    {
        if (startTimer == true)
        {

            t -= Time.deltaTime;


            string minutes = ((int)t / 60).ToString();
            string seconds = (t % 60).ToString("f2");

            if (minutes == "0")
                onlyShowingSeconds = true;


            if (onlyShowingSeconds)
                timerText.text = seconds;
            else
                timerText.text = minutes + ":" + seconds;

            if (t <= 0)
            {
                startTimer = false;
                timerText.text = "FINISH!";
				//StartCoroutine (CambioEscena ());
            }
        }
    }

    public void updateTimer()
    {
        t = 600f;
        startTimer = true;
        Debug.Log("estoy funcionando");
    }

	IEnumerator CambioEscena()
	{
		yield return new WaitForSeconds (3);
		SceneManager.LoadScene ("VicRed");
	}
}
