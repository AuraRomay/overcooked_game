﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateIngredient : MonoBehaviour {

	public GameObject table;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {



	}

	private void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.tag == "crafttable") 
		{
			this.gameObject.SetActive (false);
		}
	}

}
