﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoMesa : MonoBehaviour {

	public bool direction;
	private Vector3 speed;

	// Use this for initialization
	void Start () {

		speed = new Vector3 (0, 0, 2);
	}
	
	// Update is called once per frame
	void Update () {

		if (transform.position.z == 265) 
		{
			direction = false;
		}

		if (transform.position.z == 35) 
		{
			direction = true;
		}

		if (direction == true) 
		{
			transform.position = transform.position + speed;
		}

		if (direction == false) 
		{
			transform.position = transform.position - speed;
		}

	}
}
