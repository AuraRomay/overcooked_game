﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BotonesMenuPrincipal : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Jugar()
    {
        SceneManager.LoadScene("Gameplay");

    }

    public void Tutorial ()
    {
        SceneManager.LoadScene("Tutorial");
    }

    public void Salir()
    {
        Application.Quit();

    }

    public void Menu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}

