﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;


public class PlayerMovement : MonoBehaviour
{
	ManejoIngredientes manIng;
    public int m_PlayerNumber = 1;
    public float m_Speed = 12f;
    public float m_TurnSpeed = 180f;
    public float jump;
	public int gravedad;
    int speed = 5;
    private string m_MovementAxisName;
    private string m_TurnAxisName;
	private string m_Y;
    private Rigidbody m_Rigidbody;
    private float m_MovementInputValue;
    private float m_TurnInputValue;
	private float m_YV;
    private bool isGrounded;
    public float dash;

	public Animator anim;
	public GameObject AnimatedObject;

    private void Awake()
    {
		manIng = GetComponent<ManejoIngredientes> ();
        m_Rigidbody = GetComponent<Rigidbody>();
        isGrounded = false;
		anim = AnimatedObject.GetComponent<Animator>();
    }


    private void OnEnable()
    {
        m_Rigidbody.isKinematic = false;
        m_MovementInputValue = 0f;
        m_TurnInputValue = 0f;
    }



    private void Start()
    {
		m_MovementAxisName = "Vertical"+ m_PlayerNumber;
		m_TurnAxisName = "Horizontal"+ m_PlayerNumber;
		m_Y ="Y"+ m_PlayerNumber;
    }


    private void Update()
    {
        m_MovementInputValue = Input.GetAxis(m_MovementAxisName);
        m_TurnInputValue = Input.GetAxis(m_TurnAxisName);
		m_YV = Input.GetAxisRaw(m_Y);
        //Vector3 gravityS = new Vector3(0,GravityStrength,0);
        //Physics.gravity = gravityS;

    }


    private void FixedUpdate()
    {
        Move();
        Turn();
        Jump();
        Dash();
    }


    private void Move()
    {
        Vector3 movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;
        m_Rigidbody.MovePosition(m_Rigidbody.position + movement);

		if (m_MovementInputValue == 0) 
		{
			anim.SetBool ("Moving", false);
		}

		if (m_MovementInputValue > 0) 
		{
			anim.SetBool ("Moving", true);
		}
    }


    private void Turn()
    {
        float turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;
        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);
        m_Rigidbody.MoveRotation(m_Rigidbody.rotation * turnRotation);
    }

    private void Jump()
    {
		if (m_YV > 0 && isGrounded && manIng.Carrying == false)
        {
            //Debug.Log("jump");
            /*m_Rigidbody.AddForce(new Vector3(0, jump, 0));
            isGrounded = false;

			anim.SetBool ("Jump", true);*/

        }

		if (isGrounded == true)
		{
			anim.SetBool ("Jump", false);
		}
    }

    private void Dash()
    {
		if (m_YV > 0 && manIng.Carrying && isGrounded)
        {
             Debug.Log("jalapeño");
			m_Rigidbody.AddForce((transform.forward * -m_Speed) * dash);

        }
    }

    private void OnCollisionEnter()
    {
        // Debug.Log("popo");
        isGrounded = true;
    }
}