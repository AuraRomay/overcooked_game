﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;

public class ReciepeMaker : MonoBehaviour {

	public GameObject chilly;
	public GameObject corn;
	public GameObject cheese;
	public GameObject chicken;

	public bool ChillyHere;
	public bool CornHere;
	public bool CheeseHere;
	public bool ChickenHere;

	public int score;

	public Text scoretext;
	string scoreupdate;

	public int m_PlayerNumber;
	private string m_X;

	// Use this for initialization
	void Start () 
	{

		corn.SetActive (false);
		cheese.SetActive (false);
		chicken.SetActive (false);
		chilly.SetActive (false);
		ChillyHere = false;
		CornHere = false;
		ChickenHere = false;
		CheeseHere = false;
		score = 0;
		m_X ="X"+ m_PlayerNumber;
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		scoreupdate = score.ToString();

		if (Input.GetAxisRaw(m_X) > 0 && CheeseHere == true && CornHere == true) 
		{
			corn.SetActive (false);
			cheese.SetActive (false);
			chicken.SetActive (false);
			chilly.SetActive (false);
			ChillyHere = false;
			CornHere = false;
			ChickenHere = false;
			CheeseHere = false;

			score = score + 5;
            GamePad.SetVibration((PlayerIndex)m_PlayerNumber, 10, 10);
        }

		if (Input.GetAxisRaw(m_X) > 0 && ChickenHere == true && CornHere == true && ChillyHere == true) 
		{
			corn.SetActive (false);
			cheese.SetActive (false);
			chicken.SetActive (false);
			chilly.SetActive (false);
			ChillyHere = false;
			CornHere = false;
			ChickenHere = false;
			CheeseHere = false;

			score = score + 10;
            GamePad.SetVibration((PlayerIndex)m_PlayerNumber, 10, 10);
        }

		if (Input.GetAxisRaw(m_X) > 0 && CheeseHere == true && CornHere == true && ChillyHere == true && chicken == true) 
		{
			corn.SetActive (false);
			cheese.SetActive (false);
			chicken.SetActive (false);
			chilly.SetActive (false);
			ChillyHere = false;
			CornHere = false;
			ChickenHere = false;
			CheeseHere = false;

			score = score + 15;
            GamePad.SetVibration((PlayerIndex)m_PlayerNumber, 10, 10);
        }

		scoretext.text = scoreupdate;


	}

	private void OnTriggerEnter (Collider col)
	{
		if (col.tag == "corn") 
		{
			col.gameObject.SetActive (false);
			corn.SetActive (true);
			CornHere = true;
		}

		if (col.tag == "chicken") 
		{
			col.gameObject.SetActive (false);
			chicken.SetActive (true);
			ChickenHere = true;
		}

		if (col.tag == "chilly") 
		{
			col.gameObject.SetActive (false);
			chilly.SetActive (true);
			ChillyHere = true;
		}

		if (col.tag == "cheese") 
		{
			col.gameObject.SetActive (false);
			cheese.SetActive (true);
			CheeseHere = true;
		}


	}
}
