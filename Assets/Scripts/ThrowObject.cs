﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ThrowObject : MonoBehaviour {
    public Transform player;
    public Transform playerCara;
    public float throwForce;
    public bool hasPlayer = false;
    public bool beingCarried = false;
    public bool touched = false;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        GetDistance();

    }

    void GetDistance()
    {
        float distance = Vector3.Distance(gameObject.transform.position, player.position);
        //Debug.Log(distance);
        if (distance <= 2.5f)
        {
            hasPlayer = true;
        }
        else
        {
            hasPlayer = false;
        }
        if (hasPlayer && Input.GetButtonDown("Fire1"))
        {
            GetComponent<Rigidbody>().isKinematic = true;
            transform.parent = playerCara;
            transform.position = new Vector3(playerCara.position.x + 1, playerCara.position.y, playerCara.position.z + 1);
            beingCarried = true;
        }
        if (beingCarried)
        {
            if (touched)
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;
                touched = false;
            }
            if (Input.GetButtonDown("Fire2"))
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;
            }
        }
    }

    private void OnTriggerEnter()
    {
        if (beingCarried)
        {
            touched = true;
        }
    }
}
