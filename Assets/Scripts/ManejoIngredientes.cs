﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManejoIngredientes : MonoBehaviour {

	public GameObject corn;
	public GameObject chilly;
	public GameObject chicken;
	public GameObject cheese;
	public bool Carrying;

	public int m_PlayerNumber;
	private string m_B;
	private string m_A;
	private bool m_BV;

	// Use this for initialization
	void Start () {

		Carrying = false;
		corn.SetActive(false);
		chicken.SetActive(false);
		chilly.SetActive(false);
		cheese.SetActive(false);
		m_B ="B"+ m_PlayerNumber;
		m_A ="A"+ m_PlayerNumber;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!Carrying) 
		{
			corn.SetActive(false);
			chicken.SetActive(false);
			chilly.SetActive(false);
			cheese.SetActive(false);

		}
		if (Carrying && Input.GetAxisRaw(m_B) > 0) 
		{
			Carrying = false;
		}


	}

	private void OnTriggerStay (Collider other) 
	{
		if (other.tag == "ChickenPick" && Input.GetAxisRaw(m_A) > 0 && !Carrying) 
		{
			//ChickenPicked = true;
			Carrying = true;
			chicken.SetActive (true);
		}

		if (other.tag == "ChilliPick" && Input.GetAxisRaw(m_A) > 0 && !Carrying) 
		{
			//ChillyPicked = true;
			Carrying = true;
			chilly.SetActive(true);
		}
		if (other.tag == "CornPick" && Input.GetAxisRaw(m_A) > 0 && !Carrying) 
		{
			//CornPicked = true;
			Carrying = true;
			corn.SetActive(true);
		}
		if (other.tag == "CheesePick" && Input.GetAxisRaw(m_A) > 0 && !Carrying) 
		{
			//CheesePicked = true;
			Carrying = true;
			cheese.SetActive(true);
		}

		if (other.tag == "crafttable")
		{
			Carrying = false;
		}
	}
}
