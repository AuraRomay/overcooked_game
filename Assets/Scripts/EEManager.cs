﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EEManager : MonoBehaviour {

	int a;
	int b;
	int x;
	int y;
	bool AxisUse;
	float rotationZ = 180;
	private Vector3 rotateValue;

	// Use this for initialization
	void Start () {

		a = 0;
		b = 0;
		x = 0;
		y = 0;
		AxisUse = false;
		StartCoroutine(FrezzingEE());
	}

	// Update is called once per frame
	void Update () {

		LogicaEasterEgg();

		if (Resultado(a, b, x, y) == 37)
		{
			rotateValue = new Vector3(0, 0, rotationZ);
			transform.eulerAngles -= rotateValue;
			Start();
		}
		else if (Resultado(a, b, x, y) >= 37)
		{
			Start();
		}


		//Debug.Log("EE: " + Resultado(a, b, x, y));
	}

	void LogicaEasterEgg()
	{

		if (Input.GetAxisRaw("A1")>0)
		{
			if (!AxisUse)
			{
				a += 5;
				AxisUse = true;

			}

		}
		else if (Input.GetAxisRaw("B1") > 0)
		{
			if (!AxisUse)
			{
				b += 2;
				AxisUse = true;

			}

		}
		else if (Input.GetAxisRaw("X1") > 0)
		{
			if (!AxisUse)
			{
				b += 4;
				AxisUse = true;

			}

		}

		else if (Input.GetAxisRaw("Y1") > 0)
		{
			if (!AxisUse)
			{
				y += 6;
				AxisUse = true;

			}

		}
		else
		{
			AxisUse = false;
		}

		//if (Input.GetAxisRaw("Jump")> 0)
		//{
		//    if (!AxisUse)
		//    {
		//        y += 6;
		//        AxisUse = true;
		//    }
		//if (Input.GetAxisRaw("Jump") == 0)
		//    {
		//        Debug.Log("s");
		//        AxisUse = false;
		//    }
		//}
	}

	int Resultado(int _x, int _y, int _z, int _w)
	{
		return _x + _y + _z + _w;
	}

	IEnumerator FrezzingEE()
	{
		yield return new WaitForSeconds(5);
		Start();
	}


}
